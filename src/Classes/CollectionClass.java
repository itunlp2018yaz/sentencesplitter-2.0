package Classes;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.Files.readAllLines;

public class CollectionClass {

    private static List<DataStructure> collection = new ArrayList<>();

    public List<DataStructure> getContent() {
        return collection;
    }

    public CollectionClass() throws IOException {

        List<String> inputs = readAllLines(Paths.get("inputsForTest.txt"), StandardCharsets.UTF_8);
        List<String> outputs = readAllLines(Paths.get("outputsForTest.txt"), StandardCharsets.UTF_8);

        int i = 0;
        int j = 0;
        int ctr = 0;

        while(i< inputs.size() || j< outputs.size()) {

            DataStructure structure = new DataStructure();
            String inputIncoming = null;
            List<String> outputIncoming = new ArrayList<String>();

            if(i< inputs.size()) {

                if(!inputs.get(i).equals("-")) {
                    // nothing

                }
                else {
                    i++;
                    inputIncoming = inputs.get(i);
                    structure.string = inputIncoming;
                }
            }

            if(j< outputs.size()) {


                if(!outputs.get(j).equals("-") && !outputs.get(j).equals("--")) {
                    // nothing
                }
                else {
                    if(outputs.get(j).equals("-")) {
                        j++;
                        outputIncoming.add(outputs.get(j));
                        structure.listOfString = outputIncoming;
                    }
                    else if(outputs.get(j).equals("--")) {
                        j++;
                        outputIncoming.add(outputs.get(j));
                        j++;
                        outputIncoming.add(outputs.get(j));
                        structure.listOfString = outputIncoming;
                    }
                }
            }

            collection.add(ctr,structure);

            i++;
            j++;
            ctr++;
        }


    }
}
