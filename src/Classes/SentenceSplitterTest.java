package Classes;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SentenceSplitterTest  {                        //  this test class is mostly taken from the answer below. I asked the question.
    private SentenceSplitter sentencesplitter;                               // https://stackoverflow.com/a/51320888/10074377
    private static CollectionClass collectionClass;

    @BeforeAll
    public static void initializeCollectionClass() throws IOException {
        collectionClass = new CollectionClass();
    }
/*
    @Before
    public void initializeSentenceSplitter() throws IOException {
        sentencesplitter = new SentenceSplitter();                                      // this was producing error
    }                                                                           //therefore, I moved the initialization of sentencesplitter inside test method
*/
    @ParameterizedTest
    @MethodSource("data")
    public void testSentenceSplitterTest(DataStructure ds) {

        List<Integer> ioi = new LinkedList<>();
        Map<Integer,CustomMap> map = new LinkedHashMap<>();


        String input = ds.string;
        System.out.println("input: " + input);
        List<String> output = ds.listOfString;
        System.out.println("Expected output: " + output);
        sentencesplitter = new SentenceSplitter();
        assertEquals(output, sentencesplitter.sentenceSplit(input,ioi,map));

    }

    public static Stream<DataStructure> data() {
        return collectionClass.getContent().stream();
    }
}

