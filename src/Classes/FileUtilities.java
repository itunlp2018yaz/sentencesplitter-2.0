package Classes;

import java.awt.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class FileUtilities {

    public static String readFile(String path, Charset encoding)      // this function is copied from the internet. its purpose is to read a file efficiently.
                                                                                                                                         // link: https://stackoverflow.com/a/326440/6317336
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }



    public static String readFileLines(String path, List<Integer> ioi, int first, int last, double startTime) throws IOException {

        // first and last are corresponding indexes for this 50000 tokens of batch

        File file = new File(path);
/*
        BufferedReader bf = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(path), "UTF8"));

*/

        Scanner scanner = new Scanner(file, "UTF-8");
        StringBuffer buffer = new StringBuffer();

        String block = "";
        String temp = "";
        String previous = "";

        int ctr = -1;

        while (scanner.hasNextLine()){

            ctr++;


            if(ctr<=first)   // we need this condition to move on in the file until we reach the beginning of the batch
                temp = scanner.nextLine();

            if(ctr>=last) // after we are finished with this batch, there is non need to move on, we should quit
                break;

            if(!(ctr >= first && ctr < last)) // if our imaginary cursor (temp) is out of the batch's location
                continue;


            if(ctr == first) // here we start
                block = temp;

            if(ctr %200000 == 0) {  // indicator of progress
                System.out.println("File is being read... Number of the token is: " + ctr);
                double endTime = System.currentTimeMillis();
                System.out.println("process duration until " + ctr + ". token: " + (endTime-startTime)/1000 + " seconds");

            }
            String line = scanner.nextLine();

            ///////////////////////////////////// these conditions were affecting sentenceSplitter and causing huge errors /////////////////////////////////////
            if(previous.equals(".") && line.equals("!"))
                continue;
            else if(previous.equals(".") && line.equals("?"))
                continue;
            ///////////////////////////////////// this is an intentional and small mistake to avoid much bigger problems /////////////////////////////////////

            // some corrections //


            line = line.replace('\u00A0', ' ').replace('\u2007', ' ').replace('\u202F', ' ');

            line = line.replace('\u0091', '\'').replace('\u0092', '\'').replace('\u0093', '"').replace('\u0094', '"');

            line = line.replace('\u0096', '-');

            line = line.replace('\u008f', '|').replaceAll("\\|", "");

            line = line.replace('\u0080','€');

            line = line.replace('\u008e', '|').replaceAll("\\|", "");

            line = line.replace('\u0099', '|').replaceAll("\\|", "");

            line = line.replace('\u0082', '|').replaceAll("\\|", "");

            line = line.replace('\u0097', '|').replaceAll("\\|", "");

            line = line.replace('\u25A1', '|').replaceAll("\\|", "");

            line = line.replace('\u008D', '|').replaceAll("\\|", "");

            line = line.replace('\u0083', '|').replaceAll("\\|", "");

            line = line.replace('\u0081', '|').replaceAll("\\|", "");

            line = line.replace('\u0095', '-').trim();

            // some corrections //

            boolean dot = (line.equals("."));
            boolean qm = (line.equals("?"));
            boolean exm = (line.equals("!"));
            boolean dotx3 = (line.equals("..."));

            if(dot || qm || exm || dotx3) {

                block = block.concat(line);
                ioi.add(block.lastIndexOf(line) + line.length() - 1); // to detect seperating index

            }


            else // put an empty space between tokens other than . ? ! ...
                block = block.concat(" ".concat(line));

            previous = line; // previous token will be necessary
        }
        scanner.close();
        return block;
    }

    public static void writeToFile(List<String> sentences, String name) throws IOException {

        //PrintWriter writer = new PrintWriter(new File(name),"UTF-8");

        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(name, true), // true to append
                        StandardCharsets.UTF_8                  // Set encoding
                ));

        //FileWriter writer = new FileWriter(name);
        for(String sent: sentences) {                                                    //line by line
            //writer.print(sent + " ");                                                         //into the file

            writer.write(sent + "\n");
            //writer.print(" ");
        }
        writer.close();

    }

    public static void writeToFile2(List<String> sentences, String name) throws IOException {

        //PrintWriter writer = new PrintWriter(new File(name),"UTF-8");
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(name, true), // true to append
                        StandardCharsets.UTF_8                  // Set encoding
                ));
        //FileWriter writer = new FileWriter(name);
        for(String sent: sentences) {                                                    //line by line
            //writer.print(sent + " ");                                                         //into the file

            for(String token: sent.split("\\s+"))
                writer.write(token + "\n");
            //writer.print(" ");
        }
        writer.close();

    }

    public static int getLineNumber(FileReader file) throws IOException {

        BufferedReader bf = new BufferedReader(file);

        int lineNumber = 0;
        while(bf.readLine() != null) {

            lineNumber++;


        }


        bf.close();
        return lineNumber;
    }


    public static void writeStrToFile(String block, String name) throws IOException {

        //PrintWriter writer = new PrintWriter(new File(name),"UTF-8");

        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(name, true), // true to append
                        StandardCharsets.UTF_8                  // Set encoding
                ));

        //FileWriter writer = new FileWriter(name);
        writer.write(block);
        writer.close();

    }

    public static void readThenWrite(String path, String path2) throws IOException {

        File file = new File(path);
        Scanner scanner = new Scanner(file, "UTF-8");

        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(path2, true), // true to append
                        StandardCharsets.UTF_8                  // Set encoding
                ));


        String tokens = "";
        int ctr = 0;

        while(scanner.hasNextLine()) {
            ctr++;
            if(ctr % 10 == 0)
                System.out.println(ctr);

            if(ctr == 100)
                break;
            String temp = scanner.nextLine();
            tokens += temp;
            tokens.concat(" ");

        }
        System.out.println(tokens);
        scanner.close();

        writer.write(tokens);
        writer.close();
    }



}
