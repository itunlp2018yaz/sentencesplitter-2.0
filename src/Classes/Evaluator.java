package Classes;


import java.io.IOException;
import java.nio.charset.StandardCharsets;


import java.util.Vector;

import static Classes.FileUtilities.readFile;

public class Evaluator {

    private Vector<Integer> indexOfBreaksP = new Vector<>();            // stores breaking positions in predicted data
    private Vector<Integer> indexOfBreaksY = new Vector<>();            // stores breaking positions in correct data
    private Vector<Integer> indexEquals = new Vector<>();               // stores common positions in predicted and correct data
    private String predictions;             // stores predicted data in String object form
    private String y;                       // stores correct data in String object form

    public Vector<Integer> getIndexesP() {
        return indexOfBreaksP;
    }

    public Vector<Integer> getIndexesY() {
        return indexOfBreaksY;
    }

    public Vector<Integer> getIndexEquals() {
        return indexEquals;
    }




    public Evaluator(String predictions_in, String y_in) throws IOException {

        predictions = readFile(predictions_in, StandardCharsets.UTF_8);         //  a string for predicted data in which there is one sentence at each line
        y = readFile(y_in, StandardCharsets.UTF_8);                             // a string for correct data in which there is one sentence at each line


        StringBuilder sb;
        int indexP = predictions.indexOf("\n");                     // first index that has '\n' character
        while (indexP >= 0) {

            sb = new StringBuilder(predictions);            // StringBuilder is used to remove a character from a string
            sb.deleteCharAt(indexP);
            predictions = sb.toString();                     // updated data is back in String form (transformed from StringBuilder object)
            indexP -= 1;        // go one char back to detect breaking index because 1 char is deleted
            indexOfBreaksP.add(indexP);             // to store breaking indexes for predictions string
            indexP = predictions.indexOf("\n", indexP + 1);     // next index that has '\n' character
        }

        int indexY = y.indexOf("\n");
        while (indexY >= 0) {                                    // same

            sb = new StringBuilder(y);
            sb.deleteCharAt(indexY);                                    // as                                    // but for y string
            y = sb.toString();
            indexY -= 1;
            indexOfBreaksY.add(indexY);                                     // above
            indexY = y.indexOf("\n", indexY + 1);
        }
    }


    public void compare() {

        int tp = 0;                         // stores # of true positive sentences (which are seperated and correct)
        int fp = 0;                         // stores # of false positive sentences (which are seperated but wrong, should not have been seperated)
        int fn = 0;                         // stores # of false negative sentences (which are not seperated and wrong, should have been seperated)
        double precision; // rate of tp in predicted data
        double recall;    // rate of tp in actual data
        double f_measure; // something like final score

        for (int i = 0; i < indexOfBreaksP.size(); i++) {
            for (int j = 0; j < indexOfBreaksY.size(); j++) {

                if (indexOfBreaksP.get(i).equals(indexOfBreaksY.get(j))) {                  // loops to detect true positive indexes
                    indexEquals.add(indexOfBreaksP.get(i));
                    tp++;
                    break;
                }
            }
        }

        System.out.println("# of predicted sentences: "+indexOfBreaksP.size());
        System.out.println("# of actual sentences: "+indexOfBreaksY.size());

        fp = indexOfBreaksP.size() - tp;
        fn = indexOfBreaksY.size() - tp;

        precision = tp / (double) (tp + fp);
        recall = tp / (double) (tp + fn);
        f_measure = 2 * precision * recall / (precision + recall);

        String p = String.format("%.2f", precision*100);
        String r = String.format("%.2f", recall*100);
        String f = String.format("%.2f", f_measure*100);

        System.out.println("# of true positive sentences: " + tp);
        System.out.println("# of false positive sentences: " + fp);
        System.out.println("# of false negative sentences: " + fn);
        System.out.println("precision: " + p + " %");
        System.out.println("recall: " + r + " %");
        System.out.println("f_measure: " + f + " %");

    }

}







