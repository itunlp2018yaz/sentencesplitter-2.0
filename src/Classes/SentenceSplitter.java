package Classes;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.nio.file.Files.readAllLines;

public class SentenceSplitter {


    private static List<String> abbr = null;

    private Vector<Integer> indexes = new Vector<>();               // will store seperation positions
    private Vector<Integer> quotationParanthesisIndexes = new Vector<>();       // will store positions of quotation marks and paranthesis in which sentences will not be seperated
    private Vector<Integer> wannabeDeletedIndexes = new Vector<>();          // will store positions where sentences end but these sentences are inside of a quotation or paranthesis

    static {
        try {
            abbr = readAllLines(Paths.get("abbr.txt"), StandardCharsets.UTF_8);     // file of abbreviations
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    public List<String> sentenceSplit(String str, List<Integer> ioi, Map<Integer,CustomMap> map) {

        List<String> sentences = new ArrayList<String>();                    // list of string variables to store seperated sentences

        indexes.add(0);        // this will be useful while seperating first sentence


        caseDot(str);                                              //   cases
        caseDot2(str);                                           //     to detect
        caseQuestionExclamation(str);                             //    indexes
        caseEllipsis(str);                                          //  where
        //caseQuotationParanthesis(str);                             //   sentences
        caseParanthesis(str);                                    //     end


        Collections.sort(indexes);  // sorting before last changes


        detectIndexesBetweenQuotationParanthesis();         // to detect breaking indexes between quotation marks/paranthesis and store them


        deleteIndexesBetweenQuotationParanthesis();         // to delete breaking indexes between quotation marks/paranthesis



        if(indexes.size() == 1)
            sentences.add(str);
        else {

            int ctr = -1;

            for(int i=0;i<indexes.size()-1;i++) {                   // loop to seperate sentences with corresponding indexes from block text "str"


                ctr++;

                String sentence = str.substring((int)indexes.get(i),(int)indexes.get(i+1));

                sentences.add(sentence);

                CustomMap temp = new CustomMap();
                boolean anythingAdded = false;

                for (int j=0;j<ioi.size();j++)  {

                    if(ioi.get(j) >= indexes.get(i) && ioi.get(j) < indexes.get(i+1)) {
                        anythingAdded = true;



                        if(indexes.get(i+1) - ioi.get(j) > 2) {
                            temp.exception = (indexes.get(i+1) - 2);
                            temp.listofinteger.add(ioi.get(j));
                        }

                        else
                            temp.listofinteger.add(ioi.get(j));
                        // System.out.println(ioi.get(j));

                    }

                    else if(ioi.get(j) >= indexes.get(i+1))
                        break;
                }


                if(anythingAdded) {

                    temp.theone = true;
                    map.put(ctr, temp);

                }
                else {

                    int totalLength = 0;

                    for (int k=0;k<=ctr;k++) {
                        totalLength += sentences.get(k).length();
                    }


                    temp.listofinteger.add(totalLength -2);
                    temp.theone = false;
                    map.put(ctr, temp);
                }


            }
        }




        return sentences;
    }







    //////////////////////////////////////////////////////////////////////////////  FIRST CASE IS FOR DOT ( . ) /////////////////////////////////////////////////////////////////////////

    public void caseDot(String str) {

        Pattern p_catchFirstCase = Pattern.compile("([a-zA-Z'çğışöüÇĞİŞÖÜ&&[^IXV]]{2,5}[ ]{0,1}[.]{1,3}[\\s])|([.]{1,3}[ ]$)|([.]{1,3}$)|($)|" +  // dots after abbreviations, dots after ending words of the sentences
                "([0-9]+[.][0-9]+\\s)|" +                                       // dots between numbers    etc: 23.342.343 nüfuslu ülke.
                "(([0-9]{1,3}[.])+\\s[a-zçğışöü])|"  +                      // dots after ranking numbers etc: sınavda 1.232. oldu.
                "([o][ ]{0,1}[.][\\sA-Z])|" +                            // special case: dots after the word "o" etc: Gölgelerin içinden çıkan kişidir o.

                "([XVI]{1,4}[ ]{0,1}[.]\\s[A-ZÇĞİŞÖÜ])");              // dots after roman numbers etc: II. Mehmet IV. Henry

        Matcher m_catchFirstCase = p_catchFirstCase.matcher(str);


        String regexCheckFirstCase="";
        for(int it=0;it<abbr.size();it++) {                 // loop to add abbreviations to regex from abbr.txt file

            if(it==(int)abbr.size()-1) {
                regexCheckFirstCase += "(" + abbr.get(it) + "[.])";
                break;
            }
            regexCheckFirstCase += "(" + abbr.get(it) + "[.])|";
        }


        regexCheckFirstCase += "|([0-9]+[.][0-9]+[ ]+)|" +   // not gonna be seperated if this piece of expression (regular numbersin the middle of a sentence) is found
                "[XVI]{1,4}[.]|" +                                          // not gonna be seperated if this piece of expression (roman numbers) is found
                "[a-zA-Z'çğışöüÇĞİŞÖÜ&&[^IXV]]{2,4}[ ]{0,1}[.][.]|" +               // not gonna be seperated if this piece of expression (<endword>..) is found. reason behind this is that detecting ellipsis points is not the purpose of this case. ellipsis detection is being handled in its own case (case 4th).
                                                                                        //this avoids this situation: sentence.
                                                                                       //                            ..
                "(([0-9]{1,3}[.])+\\s[a-zçğışöü])";                     // not gonna be seperated if this piece of expression (ranking numbers) is found


        while(m_catchFirstCase.find()) {

            Pattern p_checkFirstCase = Pattern.compile(regexCheckFirstCase);
            Matcher m_checkFirstCase = p_checkFirstCase.matcher(m_catchFirstCase.group());          // finds the exact part where sentence does not end

            if(m_checkFirstCase.find()){                                            // checks if there is a reason to not seperate
                // nothing
            }

            else {
                if(!indexes.contains(m_catchFirstCase.start() + m_catchFirstCase.group().length()))      //avoids multiple seperation at one index
                indexes.add(m_catchFirstCase.start() + m_catchFirstCase.group().length());  // adds the exact index where sentence will be seperated
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////  SECOND CASE IS ALSO FOR DOT ( . ) /////////////////////////////////////////////////////////////////////////

    public void caseDot2(String str) {

        Pattern p_catchSecondCase = Pattern.compile("(([0-9]{1,3}[.])+\\s(?=[A-ZÇĞİŞÖÜ0-9\"-]))|([0-9]+[ ]$)|([0-9]+$)");   // numbers at the end of the sentence
        Matcher m_catchSecondCase = p_catchSecondCase.matcher(str);                                     // catches the numbers at the end of the sentence

        String regexCheckSecondCase = "(([0-9]{1,3}[.])+\\s)";                     // exact part

        while(m_catchSecondCase.find()) {

            Pattern p_checkSecondCase = Pattern.compile(regexCheckSecondCase);
            Matcher m_checkSecondCase = p_checkSecondCase.matcher(m_catchSecondCase.group());           // finds the exact part where sentence ends

            if(m_checkSecondCase.find()) {
                if(!indexes.contains(m_catchSecondCase.start() + m_catchSecondCase.group().length()))      //avoids multiple seperation at one index
                    indexes.add(m_catchSecondCase.start() + m_catchSecondCase.group().length() );       // adds the exact index where sentence will be seperated
            }

        }
    }

    ///////////////////////////////////////////////////////////////////  THIRD CASE IS FOR QUESTION MARK ( ? ) and EXCLAMATION MARK ( ! ) /////////////////////////////////////////////////////////////////////////

    public void caseQuestionExclamation(String str) {

        Pattern p_catchThirdCase = Pattern.compile("([?!][ ]{0,1}(?=[A-ZÇĞİŞÖÜ0-9\"-]))|([?!]+[ ]*$)|([?!]+[ ]$)|([?!]+$)");     // question and exclamation marks at the end of the sentence
        Matcher m_catchThirdCase = p_catchThirdCase.matcher(str);                                   // catches these marks at the end of the sentence

        String regexCheckThirdCase = "([?!])";                   // exact part

        while(m_catchThirdCase.find()) {

            Pattern p_checkThirdCase = Pattern.compile(regexCheckThirdCase);
            Matcher m_checkThirdCase = p_checkThirdCase.matcher(m_catchThirdCase.group());              // finds the exact part where sentence ends

            if(m_checkThirdCase.find()) {
                if(!indexes.contains(m_catchThirdCase.start() + m_catchThirdCase.group().length()))      //avoids multiple seperation at one index
                    indexes.add(m_catchThirdCase.start() + m_catchThirdCase.group().length() );              // adds the exact index where sentence will be seperated
            }

        }
    }

    /////////////////////////////////////////////////////////////////// FOURTH CASE IS FOR ELLIPSIS ( . . . ) /////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void caseEllipsis(String str) {

        Pattern p_catchFourthCase = Pattern.compile("([!?.][.][.]\\s{0,1}(?=[A-ZÇĞİŞÖÜ0-9\"-]))|([!?.][.][.]+[ ]$)|([!?.][.][.]+$)");          // ellipsis at the end of the sentence
        Matcher m_catchFourthCase = p_catchFourthCase.matcher(str);                             // catches ellipsis at the end of the sentence

        String regexCheckFourthCase = "([!?.][.][.][ ])";              // exact part

        while(m_catchFourthCase.find()) {

            Pattern p_checkFourthCase = Pattern.compile(regexCheckFourthCase);
            Matcher m_checkFourthCase = p_checkFourthCase.matcher(m_catchFourthCase.group());       // finds the exact part where sentence ends

            if(m_checkFourthCase.find()) {
                if(!indexes.contains(m_catchFourthCase.start() + m_catchFourthCase.group().length()))      //avoids multiple seperation at one index
                    indexes.add(m_catchFourthCase.start() + m_catchFourthCase.group().length());            // adds the exact index where sentence will be seperated
            }

        }
    }

    /////////////////////////////////////////////////////////////////// FIFTH CASE IS FOR QUOTATION MARK ( " " ) and PARANTHESIS ( ( ) ) /////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void caseQuotationParanthesis(String str) {

        Pattern p_catchFifthCase = Pattern.compile("([\"“](.*?)[\"”])|" +                    // quotation marks and anything between these TWO quotation marks
                "([(](.*?)[)])");                                           // paranthesis and anything between these TWO paranthesis
        Matcher m_catchFifthCase = p_catchFifthCase.matcher(str);

        String regexCheckFifthCase = "([\"“][\\W\\w]*[\"”])|" +                     // exact part
                "([(][\\W\\w]*[)])";                            // exact part

        while(m_catchFifthCase.find()) {


            Pattern p_checkFifthCase = Pattern.compile(regexCheckFifthCase);
            Matcher m_checkFifthCase = p_checkFifthCase.matcher(m_catchFifthCase.group());      // finds the exact part where sentence ends

            if(m_checkFifthCase.find()) {

                quotationParanthesisIndexes.add(m_catchFifthCase.start());                  // adds the exact index where quotation/paranthesis starts
                quotationParanthesisIndexes.add(m_catchFifthCase.start() + m_catchFifthCase.group().length() );   // adds the exact index where quotation/paranthesis ends
            }
        }


        Pattern p_catchFifthCase2 = Pattern.compile("([\"][.]{0,1}\\s(?=[A-ZÇĞİŞÖÜ0-9\"-]))");            // quotation marks at the end of the sentence
        Matcher m_catchFifthCase2 = p_catchFifthCase2.matcher(str);                                     // catches quotation marks at the end of the sentence

        String regexCheckFifthCase2 = "([\"][.]{0,1})";         //exact part

        while(m_catchFifthCase2.find()) {

            Pattern p_checkFifthCase2 = Pattern.compile(regexCheckFifthCase2);
            Matcher m_checkFifthCase2 = p_checkFifthCase2.matcher(m_catchFifthCase2.group());       // finds the exact part where sentence ends

            if(m_checkFifthCase2.find()) {
                if(!indexes.contains(m_catchFifthCase2.start() + m_catchFifthCase2.group().length()))       //avoids multiple seperation at one index
                    indexes.add(m_catchFifthCase2.start() + m_catchFifthCase2.group().length());        // adds the exact index where sentence will be seperated
            }

        }
    }

    /////////////////////////////////////////////////////////////////// SIXTH CASE IS FOR PARANTHESIS ( ( ) ) /////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void caseParanthesis(String str) {

        Pattern p_catchSixthCase = Pattern.compile("([)][.]\\s(?=[A-ZÇĞİŞÖÜ0-9\"-])|([)]+[ ]$)|([)]+$))");          // paranthesis at the end of the sentence
        Matcher m_catchSixthCase = p_catchSixthCase.matcher(str);                           // catches paranthesis at the end of the sentence

        String regexCheckSixthCase = "([)][.])";         //exact part

        while(m_catchSixthCase.find()) {


            Pattern p_checkSixthCase = Pattern.compile(regexCheckSixthCase);
            Matcher m_checkSixthCase = p_checkSixthCase.matcher(m_catchSixthCase.group());              // finds the exact part where sentence ends

            if(m_checkSixthCase.find()) {
                if(!indexes.contains(m_catchSixthCase.start() + m_catchSixthCase.group().length()))     //avoids multiple seperation at one index
                    indexes.add(m_catchSixthCase.start() + m_catchSixthCase.group().length());          // adds the exact index where sentence will be seperated
            }

        }
    }


    public void detectIndexesBetweenQuotationParanthesis() {

        for(int i=0;i<quotationParanthesisIndexes.size();i+=2) {  // i+=2 because there are 2 quotation marks or paranthesis; one is at the beginning, the other is at the end
            for(int j=0;j<indexes.size();j++) {

                if((int)quotationParanthesisIndexes.get(i) < (int)indexes.get(j) &&  (int)indexes.get(j) < (int)quotationParanthesisIndexes.get(i+1)) {
                    wannabeDeletedIndexes.add(j);
                }
            }
        }
    }

    public void deleteIndexesBetweenQuotationParanthesis() {

        for(int j=wannabeDeletedIndexes.size() - 1;j>=0;j--) {

            indexes.removeElementAt(wannabeDeletedIndexes.get(j));
        }
    }



}
