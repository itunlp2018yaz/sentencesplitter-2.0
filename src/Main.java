import Classes.Evaluator;
import Classes.FileUtilities;
import Classes.SentenceSplitter;
import Classes.CustomMap;


import java.awt.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import java.util.*;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {


        double startTime = System.currentTimeMillis();


        for(int filenumber=1; filenumber<=213;filenumber++) { // 213 was the number of files each of which contains 1 million tokens of the main while (which has approx. 213 millions of tokens)

            double startTimePerFile = System.currentTimeMillis();

            String extension = String.valueOf(filenumber);
            String filePath = "files/file" + extension + ".txt"; //constructing the filepath for each file
            FileReader file = new FileReader(filePath);


            int numberOfLines = FileUtilities.getLineNumber(file);
            System.out.println(numberOfLines);

            file.close();

            System.out.println("file number: " + filenumber);

            for (int divider=0; divider<numberOfLines; divider+=50001) { //to speed up the process i divided the subtext into pieces of 50000 tokens

                int batch = 50000;

                if(divider+50000 > numberOfLines)
                    batch = numberOfLines - divider;

                List<Integer> ioi = new ArrayList<>(); // seperating indexes are stored

                Map<Integer,CustomMap> map = new LinkedHashMap<>();

                String block = FileUtilities.readFileLines(filePath, ioi, divider, divider+batch, startTimePerFile); // converts 50000 tokens into a block of text which contains these 50000 tokens in a corrected string form


                List<String> sentences = new ArrayList<String>();       // "sentences" will store sentences of the sample

                List<String> sentencesModified = new ArrayList<String>();

                SentenceSplitter sample = new SentenceSplitter();

            /*
            for(String part:parts) {

                SentenceSplitter sample = new SentenceSplitter();               // SentenceSplitter object to use sentenceSplit function





                String unitedVersion = "";

                for (String currSplitPart : sample.sentenceSplit(part)) {
                    unitedVersion += currSplitPart + " ";
                }

                sentences.add(unitedVersion.trim());


                sentences.addAll(sample.sentenceSplit(part));

            }
                    */


                sentences = sample.sentenceSplit(block,ioi,map); // sentences are set and ready to be written.

                // below operations allow us to fix a bug related to indexes of tokens (. ! ? ...) that we needed to append to the previous token



                ArrayList<Integer> keys = new ArrayList<Integer>(map.keySet());

                for (int i = keys.size() - 1; i > 0; i--) {


                    Integer key = keys.get(i);

                    List<Integer> values = map.get(key).listofinteger;

                    for(int j=values.size()-1;j >= 0;j--) {

                        if(map.get(key-1).listofinteger.size() == 0) {
                            map.remove(key);
                            continue;
                        }
                        Integer element;

                        if(map.get(key-1).exception != 0)
                            element = values.get(j) - map.get(key-1).exception - 2;
                        else
                            element = values.get(j) - map.get(key-1).listofinteger.get(map.get(key-1).listofinteger.size()-1) - 2;

                        values.set(j,element);
                    }

                }


                //        for(Map.Entry<Integer, List<Integer>> entry: map.entrySet()) {
                //
                //
                //            Integer key = entry.getKey();
                //            List<Integer> value1 = entry.getValue();
                //            List<Integer> value2 = mapT.get(key);
                //            System.out.println(key);
                //
                //            if(key == 0)
                //                continue;
                //
                //            for(int j=value2.size()-1;j >= 0;j--) {
                //                System.out.println(value2.get(j));
                //                System.out.println(map.get(key-1).get(map.get(key-1).size()-1));
                //                Integer element = value2.get(j) - map.get(key-1).get(map.get(key-1).size()-1) - 2;
                //                System.out.println(element);
                //                value2.set(j,element);
                //                System.out.println(map);
                //                System.out.println(mapT);
                //            }
                //        }


                // operations below allow us to redo what we did


                int ctr = 0;
                for(String sent:sentences) {
                    StringBuilder sb = new StringBuilder(sent);

                    if(map.get(ctr) == null) {
                        ctr++;
                        continue;
                    }


                    if(map.get(ctr).theone){



                        for(int j=map.get(ctr).listofinteger.size()-1;j >= 0;j--) {

                            sb.insert(map.get(ctr).listofinteger.get(j)," ");
                        }



                        sent = sb.toString();
                        sent = sent.replace(".. ."," ...");


                    }
                    sentencesModified.add(sent);

                    ctr++;
                }



                FileUtilities.writeToFile(sentencesModified, "SampleOutput.txt");


                FileUtilities.writeToFile2(sentencesModified, "SampleOutputCompareWithOriginal.txt");


            /*
            String original = FileUtilities.readFile("outputTrimTest_ecemFix.2.txt", StandardCharsets.UTF_8);

            String splittedOriginal = FileUtilities.readFile("splittedTrimFixLast.txt", StandardCharsets.UTF_8);

            */

            }

            double endTimePerFile = System.currentTimeMillis();

            System.out.println("File number " + filenumber + " process duration for: " + (endTimePerFile-startTimePerFile)/1000 + " seconds");

            for(int f=0; f<filenumber; f++) Toolkit.getDefaultToolkit().beep();

        }





        double endTime = System.currentTimeMillis();

        System.out.println("process duration: " + (endTime-startTime)/1000 + " seconds");



        /*

        Evaluator eva = new Evaluator("splitted.txt", "tokenizedTextBulut.txt");

        eva.compare();

        double endTime2 = System.currentTimeMillis();

        System.out.println("process duration with evaluation: " + (endTime2-startTime)/1000 + " seconds");

        */

    }


}

